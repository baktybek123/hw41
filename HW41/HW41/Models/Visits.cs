﻿using HW41.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW41.Models
{
    public class Visits
    {
        public int ID { get; set; }
        public VisitType visitType { get; set; }
        public DateTime DateOfVisit { get; set; }
        public VisitStatus VisitStatus { get; set; }
        public decimal Price { get; set; }
        [ForeignKey("Patient")]
        public int PatientId { get; set; }

        public virtual Patient Patient { get; set; }

    }
}
